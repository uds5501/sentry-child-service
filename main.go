package main

import (
	"context"
	"encoding/hex"
	"fmt"
	"log"
	"net/http"
	"regexp"
	"time"

	"github.com/getsentry/sentry-go"
	sentrygin "github.com/getsentry/sentry-go/gin"
	"github.com/gin-gonic/gin"
)
var sentryTracePattern = regexp.MustCompile(`^([[:xdigit:]]{32})-([[:xdigit:]]{16})(?:-([01]))?$`)

func updateFromSentryTrace(s *sentry.Span, header []byte) {
	m := sentryTracePattern.FindSubmatch(header)
	fmt.Println("inparsing: ", m)
	if m == nil {
		// no match
		return
	}
	_, _ = hex.Decode(s.TraceID[:], m[1])
	_, _ = hex.Decode(s.ParentSpanID[:], m[2])
	fmt.Println("Done modifying the span")
}
func ContinueFromRequest(r *http.Request) sentry.SpanOption {
	return func(s *sentry.Span) {
		trace := r.Header.Get("sentry-trace")
		fmt.Println("TRACE:", trace)
		updateFromSentryTrace(s, []byte(trace))
	}
}
func main() {
	log.Println("Welcome to Kid Go service")
	shutdown := initTracer()
	defer shutdown()
	r := gin.New()
	r.Use(sentrygin.New(sentrygin.Options{}))

	r.GET("/", func(c *gin.Context) {
		c.Request.Header["sentry-trace"] = []string{c.Request.Header.Get("Sentry-Trace")}
		fmt.Println(c.Request.Header.Get("sentry-trace"))
		//spanOptions := ContinueFromRequest(c.Request)
		spanOptions := sentry.ContinueFromRequest(c.Request)
		span := sentry.StartSpan(c.Request.Context(), "kidGetUser", spanOptions, sentry.TransactionName("Kid In main"))
		//spanOptions(span)

		//span := sentry.StartSpan(c.Request.Context(), "kidGetUser", sentry.TransactionName("Kid In main"))
		defer span.Finish()
		name := getAwesome(span.Context())
		c.JSON(http.StatusOK, gin.H{
			"code": http.StatusOK,
			"message": name,
		})
	})
	_ = r.Run(":8089")
}

func initTracer() func() {
	log.Println("Initialising tracer")
	err := sentry.Init(sentry.ClientOptions{
		// Either set your DSN here or set the SENTRY_DSN environment variable.
		Dsn: "https://08699f6677594704adc57e7a1f3f54aa@sentry.trell.co/29",
		// Either set environment and release here or set the SENTRY_ENVIRONMENT
		// and SENTRY_RELEASE environment variables.
		Environment: "",
		Release:     "Kid-project",
		TracesSampleRate: 1,
		// Enable printing of SDK debug messages.
		// Useful when getting started or trying to figure something out.
		Debug: true,
	})
	if err != nil {
		log.Fatalf("sentry.Init: %s", err)
	}
	return func() {
		sentry.Flush(2 * time.Second)
	}
}

func getAwesome(c context.Context) string {
	// Pass the built-in `context.Context` object from http.Request to OpenTelemetry APIs
	// where required. It is available from gin.Context.Request.Context()
	span := sentry.StartSpan(c, "kidGetAwesome")

	//ctx, span := tracer.Start(c.Request.Context(), "getUser", oteltrace.WithAttributes(attribute.String("id", id)))
	defer span.Finish()
	time.Sleep(200 * time.Millisecond)
	return "Imma Uddeshya"
}
func handleErr(err error, message string) {
	if err != nil {
		log.Fatalf("%s: %v", message, err)
	}
}
