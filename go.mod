module trell/sentry-sample-kid

go 1.16

require (
	github.com/getsentry/sentry-go v0.11.0
	github.com/gin-gonic/gin v1.7.2
)
